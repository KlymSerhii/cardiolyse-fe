import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Subscription } from 'rxjs/Subscription';
import { TokenService } from '../services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit, OnDestroy {
  tokenGetted: Subscription;
  tokenOk = false;

  constructor(private loginService: LoginService,
              private tokenService: TokenService) {
  }

  ngOnInit() {
    this.subscribeOnToken();
  }

  ngOnDestroy() {
    this.tokenGetted.unsubscribe();
  }

  login(): void {
    this.loginService.login();
  }

  subscribeOnToken(): void {
    this.tokenGetted = this.tokenService.tokenAccepted()
                           .subscribe(() => {
                             this.tokenOk = true;
                             console.log(this.tokenService.getToken());
                           });
  }

}
