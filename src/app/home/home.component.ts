import { Component, OnDestroy, OnInit } from '@angular/core';
import { TestService } from '../services/test.service';
import { Subscription } from 'rxjs/Subscription';
import { ReportModel } from '../models/report.model';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { TokenService } from '../services/token.service';
import { GeneralModel } from '../models/general.model';

const URL = 'https://cardiolyse-test.herokuapp.com/upload';
// const URL = 'http://localhost:3000/upload';
import {namesObj} from '../mock/namesObj';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.css' ]
})
export class HomeComponent implements OnInit, OnDestroy {
  namesObj = namesObj;
  reportSubscription: Subscription;
  reports: ReportModel[] = [];
  public uploader: FileUploader = new FileUploader({
    url: URL,
    itemAlias: 'csv'
  });

  uo: FileUploaderOptions = {};

  constructor(private testService: TestService, private tokenService: TokenService) {
  }

  ngOnInit() {
    this.reportSubscription = this.testService.reportGenerated()
                                  .subscribe((report: ReportModel) => {
                                    this.reports.push(report);
                                  });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
      this.uo.headers = [ {
        name: 'token',
        value: this.tokenService.getToken()
      }
      ];
      this.uploader.setOptions(this.uo);
    };

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.onCompleteSingleFile(response);
    };
  }

  onCompleteSingleFile(response: any): void {
    const transformedServerResponse: GeneralModel = JSON.parse(response) as GeneralModel;
    transformedServerResponse.key = '';
    transformedServerResponse.ecgId = '';
    transformedServerResponse.time = '';
    this.testService.getLocalTemplate(1)
        .subscribe((template: GeneralModel) => {
          template.key = '';
          template.ecgId = '';
          template.time = '';

          this.testService.informReportGenerated(this.testService.compare(transformedServerResponse, template));
        });
  }

  ngOnDestroy() {
    this.reportSubscription.unsubscribe();
  }

  startTesting(): void {
    this.testService.sendFile(1);
  }

}
