import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { TokenService } from './token.service';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

    private credencials = {
        username: '112motion',
        password: '0002279cd3f6c348d445e189500d6455555e89ca6486f8b742f82e034cf8a3ce'
    };

    private loginHeaders = new Headers({
        'Content-Type': 'application/json',
    });
    private loginUrl = 'https://cardiolyse-test.herokuapp.com/';

    constructor(private http: Http,
                private tokenService: TokenService) {
    }

    login(): void {
        let body = JSON.stringify({ req: 'login' });
        this.http.post(this.loginUrl + 'login', body, { headers: this.loginHeaders })
                   .map(res => res.json())
                   .subscribe((response) => {
                       console.log(response);
                       this.tokenService.setToken(response.token);
                   });
    }

}
